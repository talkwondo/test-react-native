import {
  GET_COUNTRIES_PENDING,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRIES_FAILED,
  FAVORITE,
  CHANGE_TEXT,
} from '../Actions/types.js';

const INITIAL_STATE = {
  countriesList: [],
  loading: false,
  error: '',
  favorite: [],
  searchText: '',
};

export const showCounries = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_COUNTRIES_PENDING:
      return {...state, loading: true};
    case GET_COUNTRIES_SUCCESS:
      return {...state, countriesList: action.payload, loading: false};
    case GET_COUNTRIES_FAILED:
      return {...state, error: action.payload, loading: false};
    default:
      return state;
  }
};

export const updateFavorite = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case FAVORITE:
      return {...state, favorite: [...state.favorite, action.payload]};
    default:
      return state;
  }
};

export const searchCountries = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case CHANGE_TEXT:
      return {...state, searchText: action.payload};
    default:
      return state;
  }
};
