import {
  GET_COUNTRIES_PENDING,
  GET_COUNTRIES_SUCCESS,
  GET_COUNTRIES_FAILED,
  FAVORITE,
  CHANGE_TEXT,
} from './types.js';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';

export const fetchCountires = () => dispatch => {
  dispatch({type: GET_COUNTRIES_PENDING});
  NetInfo.fetch().then(state => {
    if (state.isConnected) {
      fetch('https://restcountries.eu/rest/v2/all')
        .then(response => response.json())
        .then(data => {
          dispatch({type: GET_COUNTRIES_SUCCESS, payload: data});
          return data;
        })
        .then(data => AsyncStorage.setItem('@countries', JSON.stringify(data)))
        .catch(err => dispatch({type: GET_COUNTRIES_FAILED, payload: err}));
    } else {
      AsyncStorage.getItem('@countries').then(data => {
        dispatch({type: GET_COUNTRIES_SUCCESS, payload: JSON.parse(data)});
      });
    }
  });
};

export const setFavorite = country => ({
  type: FAVORITE,
  payload: country,
});

export const searchCountry = text => ({
  type: CHANGE_TEXT,
  payload: text,
});
