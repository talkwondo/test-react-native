import React from 'react';
import {NavigationContainer, DarkTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import {
  showCounries,
  updateFavorite,
  searchCountries,
} from '../Reducers/Reducer';
import {Button} from 'react-native';
import ReduxThunk from 'redux-thunk';
import Worldclass from './Worldclass.js';
import Info from '../Containers/Info';
import Favorite from '../Containers/Favorite';
import 'react-native-gesture-handler';

const rootReducer = combineReducers({
  showCounries,
  updateFavorite,
  searchCountries,
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));
const Stack = createStackNavigator();

// eslint-disable-next-line no-undef
const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer theme={DarkTheme}>
        <Stack.Navigator initialRouteName="worldclass">
          <Stack.Screen
            name="worldclass"
            component={Worldclass}
            options={({navigation}) => ({
              headerRight: () => (
                <Button
                  onPress={() => navigation.navigate('favorite')}
                  title="Favorite"
                />
              ),
              title: 'World Class',
            })}
          />
          <Stack.Screen
            name="info"
            component={Info}
            options={{title: 'Country Info'}}
          />
          <Stack.Screen
            name="favorite"
            component={Favorite}
            options={{title: 'Country Favorite'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};
export default App;
