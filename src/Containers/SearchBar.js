import React from 'react';
import {TextInput, StyleSheet, View} from 'react-native';

const SearchBar = props => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInput}
        placeholder={'Search Country...'}
        autoCorrect={false}
        autoCapitalize="none"
        onChangeText={props.onSearchCountry}
      />
    </View>
  );
};

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    height: 45,
  },
  textInput: {
    textAlign: 'left',
    fontSize: 20,
    marginLeft: 5,
  },
});
