/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {SvgUri} from 'react-native-svg';
import { ScrollView } from 'react-native-gesture-handler';

const Info = ({route}) => {
  const data = route.params.item;
  const handleTitle = () => {
  // eslint-disable-next-line react-native/no-inline-styles
  return ( data.name.length > 11) ? <Text style={{ color: 'white',
  fontSize: 20, marginTop: 20}}>{data.name.split('(')[0].trim()}</Text>
  : <Text style={styles.h1}>{data.name.split('(')[0].trim()}</Text>;
  };
  const handleSecTitle = () => {
      // eslint-disable-next-line react-native/no-inline-styles
      return (data.name.length > 18) ? <Text style={{color: 'white',
      fontSize: 18, marginTop: 20}}>{data.nativeName}</Text> : <Text style={styles.h2}>{data.nativeName}</Text>
  }
  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <View style={styles.column}>
        {handleTitle()}
        {handleSecTitle()}
        </View>
        <View style={styles.flag}>
          <SvgUri width="100" height="60" uri={data.flag} />
        </View>
      </View>
      <View style={styles.column}>
        <Text style={styles.p}>{`The Capital:    ${data.capital}`}</Text>
        <Text style={styles.p}>{`Time-Zone:    ${data.timezones[0]}`}</Text>
        <Text style={styles.p}>{`Population:     ${data.population / 1000000} M`}</Text>
      </View>
      <View style={styles.divider} />
      <View style={styles.row}>
          <Text style={styles.h2}>Languges</Text>
      </View>
      <View style={styles.lang}>
          {data.languages.map(element => {
            return <Text style={styles.lang} key={element.name}>{`${element.name}`}</Text>
          })}
      </View>
        <Text style={[styles.p, {marginTop: 20}]}>translations</Text>
        <View style={styles.column}>
            <ScrollView style={styles.translations}>
            {Object.values(data.translations).map(element => {
              // eslint-disable-next-line react-native/no-inline-styles
              return <Text key={Math.random() * 100} style={[styles.lang, {marginTop:5}]}>{`${element}`}</Text>;
            })}
            </ScrollView>
        </View>
    </View>
  );
};

export default Info;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#2d2d2d',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5,
  },
  column: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
  },
  h1: {
    color: 'white',
    fontSize: 40,
  },
  h2: {
    color: 'white',
    fontSize: 30,
    marginTop: 10,
    marginBottom: 10,
  },
  p: {
    fontSize: 30,
    color: 'white',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
  },
  flag: {
    marginTop: 10,
    marginRight: 5,
    aspectRatio: 1,
  },
  lang: {
    fontSize: 20,
    marginLeft: 10,
    flexDirection: 'row',
    color: 'white',
    textAlign: 'left',
  },
  divider: {
    marginTop: 30,
    marginLeft: 20,
    borderBottomColor: 'white',
    width: 150,
    borderBottomWidth: 1,
  },
  translations: {
      marginLeft: 20,
      marginTop: 10,
  },
});
