import React from 'react';
import {StyleSheet, FlatList, View, SafeAreaView, Button} from 'react-native';
import {connect} from 'react-redux';
import Card from '../Containers/Card';

const mapStateToProps = state => {
  return {
    favorite: state.updateFavorite.favorite,
  };
};

const Favorite = props => {
  console.log(props);
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        initialNumToRender={5}
        maxToRenderPerBatch={10}
        data={props.favorite}
        keyExtractor={item => item.numericCode}
        renderItem={({item}) => (
          <Card
            country={item.name}
            flag={item.flag}
            capital={item.capital}
            timeZone={item.timezones[0]}
            nav={() => props.navigation.navigate('info', {item})}
          />
        )}
      />
    </SafeAreaView>
  );
};

export default connect(
  mapStateToProps,
  null,
)(Favorite);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: 'white',
    flexDirection: 'column',
    backgroundColor: '#2d2d2d',
  },
  bigMarginTop: {
    marginTop: 50,
  },
});
