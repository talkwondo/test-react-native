import React, {useEffect} from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  FlatList,
  View,
  SafeAreaView,
  StatusBar,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import {fetchCountires, setFavorite, searchCountry} from '../Actions/Action';
import Card from '../Containers/Card';
import SearchBar from '../Containers/SearchBar';

const mapStateToProps = state => {
  return {
    countriesList: state.showCounries.countriesList,
    loading: state.showCounries.loading,
    error: state.showCounries.error,
    favorite: state.updateFavorite.favorite,
    searchText: state.searchCountries.searchText,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchCountires: () => dispatch(fetchCountires()),
    onAddToFavorite: item => dispatch(setFavorite(item)),
    onSearchCountry: text => dispatch(searchCountry(text)),
  };
};

const filterSearch = props => {
  const filteredResults = props.countriesList.filter(country => {
    return country.name.toLowerCase().includes(props.searchText.toLowerCase());
  });
  return props.searchText.length > 1 ? filteredResults : props.countriesList;
};

const helperFavorite = (item, props) => {
  const result = props.favorite.some(favorite => {
    return item.name === favorite.name;
  });
  if (result) {
    Alert.alert(`${item.name} has already added to your favorite`);
  } else {
    Alert.alert(`${item.name} has successfully added to your favorite`);
    props.onAddToFavorite(item);
  }
};

export const Worldclass = props => {
  if (props.error.length > 0) {
    Alert('There was an Error', props.error);
  }
  useEffect(() => {
    props.onFetchCountires();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const loadingFunc = () => {
    return props.loading ? (
      <View style={styles.bigMarginTop}>
        <ActivityIndicator size="large" />
      </View>
    ) : (
      <View />
    );
  };
  const renderItem = ({item}) => (
    <Card
      country={item.name}
      flag={item.flag}
      capital={item.capital}
      timeZone={item.timezones[0]}
      nav={() => props.navigation.navigate('info', {item})}
      favorite={() => helperFavorite(item, props)}
      favoriteButton={true}
    />
  );
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SearchBar onSearchCountry={text => props.onSearchCountry(text)} />
      <SafeAreaView style={styles.container}>
        {loadingFunc()}
        <FlatList
          initialNumToRender={5}
          maxToRenderPerBatch={10}
          data={filterSearch(props)}
          keyExtractor={item => item.numericCode}
          renderItem={renderItem}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopWidth: 1,
    borderColor: 'white',
    flexDirection: 'column',
    backgroundColor: '#2d2d2d',
  },
  bigMarginTop: {
    marginTop: 50,
  },
  favButton: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    paddingRight: 10,
    paddingBottom: 10,
    backgroundColor: '#121212',
  },
});
// eslint-disable-next-line prettier/prettier
export default connect(mapStateToProps, mapDispatchToProps)(Worldclass);
