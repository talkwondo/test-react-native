import React from 'react';
import {Dimensions, StyleSheet, View, Text, Button} from 'react-native';
import {SvgUri} from 'react-native-svg';

const {height} = Dimensions.get('window');
const Card = props => {
  const checkParentheses = () => {
    const regex = props.country.match(/\((.*?)\)/);
    const length = props.country.length;
    if (regex) {
      const str = props.country.split('(');
      return regex[0].length > 14 ? (
        <Text style={styles.bigText}>
          {str[0].trim()}
          <Text style={styles.parantheses}>{` (${str[1].slice(
            0,
            15,
          )}...)`}</Text>
        </Text>
      ) : (
        <Text style={styles.bigText}>
          {str[0].trim()}
          <Text style={styles.parantheses}>{` (${str[1].trim()}`}</Text>
        </Text>
      );
    }
    if (length > 18) {
      return (
        <Text style={styles.bigText}>{`${props.country.slice(0, 18)}...`}</Text>
      );
    }
    return <Text style={styles.bigText}>{props.country}</Text>;
  };
  const checkPropsFavorite = () => {
    if (props.favoriteButton) {
      return (
        <Button
          onPress={props.favorite}
          title="add favorite"
          accessibilityLabel="add favorite"
        />
      );
    }
  };
  return (
    <View style={styles.section}>
      <View style={styles.row}>
        {checkParentheses()}
        {/* <SvgUri style={styles.flag} width="80" height="50" uri={props.flag} /> */}
      </View>
      <View style={styles.row}>
        <View style={styles.column}>
          <Text style={styles.smallText}>{`The capital is ${
            props.capital
          }`}</Text>
          <Text style={styles.smallText}>{`The timezone is ${
            props.timeZone
          }`}</Text>
        </View>
      </View>
      <View style={styles.row}>
        <View style={styles.button}>
          <Button
            onPress={props.nav}
            title="Click for info"
            accessibilityLabel="Learn more"
          />
        </View>
        <View style={styles.button}>{checkPropsFavorite()}</View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  section: {
    height: height * 0.2,
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5,
  },
  column: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
  },
  bigText: {
    fontSize: 30,
    marginTop: 10,
    // fontFamily: 'Poppins-Bold',
    color: 'white',
  },
  flag: {
    marginTop: 10,
    marginRight: 5,
  },
  smallText: {
    fontSize: 20,
    marginTop: 10,
    color: 'white',
    // fontFamily: 'Poppins-Medium',
  },
  parantheses: {
    fontSize: 20,
    color: 'white',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 120,
    height: 50,
  },
});

export default Card;
